module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? './public'
        : '/',
    devServer: {
        overlay: {
            warnings: true,
            errors: false
        }
    },
    css: {
        requireModuleExtension: false
    }
}